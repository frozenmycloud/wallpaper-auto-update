#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <windows.h>
#include "iosfwd"
#include <qDebug>
#include "QSettings"
#include "qdir.h"
#include <QCoreApplication>
#include <QFile>
#include <QFileInfo>
#include <QList>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QSslError>
#include <QStringList>
#include <QTimer>
#include <QUrl>
#include  <QTextCodec>
#include <stdio.h>
#include <QMainWindow>
#include <QtCore>
#include <iostream>
#include <QFileDialog>
#include <iostream>
#include <QApplication>
#include <QtCore>
#include <QtGui>
#include <QTimer>
#include <QtNetwork>
#include <QTextCodec>
#include <QSystemTrayIcon>
static const int RETCODE_RESTART = 773;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setWallPaper(QString filePath);

    bool downIURL_to_picture(const QString &url, const QString &fileName);

    static QString getHtml(const QString &url);

    QString SRCdata;
    QString PURL;
    QString Picturestr;
    QStringList PURLlist;
    QStringList keylist;
    int keyIndex;
    void savePicture(QString url_name);
    void transp(QString fileName);
    QTimer isweboktimer;
    bool ipLive(QString ip, int port, int timeout = 800);
    QSystemTrayIcon *tray_icon_;
    bool saveConfig();
    void loadConfig();
    bool DirMake(const QString &path);
    void updateApp();
    QString remoteversion;
    void DeleteOldApp();
public slots:
    bool isWebOk();
    void autoScroll();
    void SlotIconIsActived(QSystemTrayIcon::ActivationReason reason);
    void setAutoStart();
    bool downIURL_to_app(const QString &url, const QString &fileName);

    bool downIURL_to_version(const QString &url);
private slots:
    void finishedURL();
    void errolURL();
    void CharckWeb();
    void setSystemTray(QString strIcon);
private:
    Ui::MainWindow *ui;
};





#endif // MAINWINDOW_H
