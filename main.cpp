#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSharedMemory shared("myapp");
        if(shared.attach())//共享内存被占用则直接返回
        {
            return 0;
        }
        shared.create(1);//共享内存没有被占用则创建UI

        MainWindow w;
        w.hide();

        w.setWindowTitle(QString("wallpaperupdata"));

        int e = a.exec();
        if(e == RETCODE_RESTART)
          {
              QProcess::startDetached(qApp->applicationFilePath(), QStringList());
              return 0;
          }
        return e;
}
