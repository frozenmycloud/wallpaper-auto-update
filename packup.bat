@echo off
echo   --------------------------------------------------------------------
echo                               准备打包QT文件
echo   --------------------------------------------------------------------
echo Setting up environment for Qt usage...
set PATH=C:/Qt/Qt5.9.0/5.9/mingw53_32/bin;C:/Qt/Qt5.9.0/5.9/mingw53_32/bin;%PATH%
cd /D C:/Qt/Qt5.9.0/5.9/mingw53_32
cd /d %~dp0
for /F %%i in ('Dir *.exe* /B') do ( 
    if not "%%i" == "filename" (
        windeployqt %%i
    )
)
echo   --------------------------------------------------------------------
echo                               打包完成
echo   --------------------------------------------------------------------
pause
